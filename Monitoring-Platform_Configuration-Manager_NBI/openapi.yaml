openapi: 3.0.2
info:
  title: Monitoring_Platform Config_Manager NBI
  version: 1.0.0
  description: >-
    High level APIs in order to configure datasources in the ANChOR monitoring
    platform.
  contact:
    name: Leonardo Lossi
    email: l.lossi@nextworks.it
paths:
  /datasources:
    summary: >-
      Endpoint to request all the datasources active inside the monitoring
      platform.
#
    get:
      tags:
        - datasource
      responses:
        '200':
          content:
            application/json:
              schema:
                type: array
                items:
                  oneOf:
                    - $ref: '#/components/schemas/owm-datasource'
                    - $ref: '#/components/schemas/nwdaf-datasource'
                    - $ref: '#/components/schemas/ransim-datasource'
                    - $ref: '#/components/schemas/starfish-datasource'
          description: List of all the active datasources inside the monitoring platform.
      operationId: getAllDatasources
      summary: Retrieves information about all the datasources.
  /datasources/{type}:
    summary: Path to deal with datasources
    get:
      tags:
        - datasource
      responses:
        '200':
          content:
            application/json:
              schema:
                type: array
                items:
                  oneOf:
                    - $ref: '#/components/schemas/owm-datasource'
                    - $ref: '#/components/schemas/nwdaf-datasource'
                    - $ref: '#/components/schemas/ransim-datasource'
                    - $ref: '#/components/schemas/starfish-datasource'
          description: Resources correctly retrieved.
        '400':
          description: Provided invalid datasource-type.
      operationId: getDatasourcesByType
      summary: Returns all the datasources of a given type.
    post:
      requestBody:
        description: Configuration of the datasource to be added
        content:
          application/json:
            schema: 
              anyOf:
                - $ref: '#/components/schemas/owm-datasource'
                - $ref: '#/components/schemas/nwdaf-datasource'
                - $ref: '#/components/schemas/ransim-datasource'
                - $ref: '#/components/schemas/starfish-datasource'                
              discriminator:
                propertyName: datasource-type
                mapping:
                  OpenWeatherMap: '#/components/schemas/owm-datasource'
                  NWDAF: '#/components/schemas/nwdaf-datasource'
                  RanSim: '#/components/schemas/ransim-datasource'
                  Starfish: '#/components/schemas/starfish-datasource'                  
        required: true
      tags:
        - datasource
      responses:
        '200':
          content:
            application/json:
              schema:
                type: integer
          description: Datasource created. Returns the Datasource UID
        '400':
          description: Provided invalid datasource configuration.
        '404':
          description: Datasource-type not found.
      operationId: addDatasourceByType
      summary: Add a new datasource of type given in the path.
    parameters:
      - name: type
        description: Datasource type.
        schema:
          $ref: '#/components/schemas/datasource-type'
        in: path
        required: true
  /datasources/{type}/id/{id}:
    summary: Path to deal with single datasources.
    get:
      tags:
        - datasource
      responses:
        '200':
          content:
            application/json:
              schema:
                oneOf:
                  - $ref: '#/components/schemas/owm-datasource'
                  - $ref: '#/components/schemas/nwdaf-datasource'
                  - $ref: '#/components/schemas/ransim-datasource'
                  - $ref: '#/components/schemas/starfish-datasource'                  
                discriminator:
                  propertyName: datasource-type
                  mapping:
                    OpenWeatherMap: '#/components/schemas/owm-datasource'
                    NWDAF: '#/components/schemas/nwdaf-datasource'
                    RanSim: '#/components/schemas/ransim-datasource'
                    Starfish: '#/components/schemas/starfish-datasource'                    
          description: Datasources retrieved.
        '400':
          description: Invalid UID provided.
      operationId: getTypedDatasourceById
      summary: get a single datasource by id.
    put:
      requestBody:
        description: New Configuration of the datasource.
        content:
          application/json:
            schema:
              anyOf:
                - $ref: '#/components/schemas/owm-editable-parameters'
                - $ref: '#/components/schemas/nwdaf-editable-parameters'
                - $ref: '#/components/schemas/ransim-editable-parameters'
                - $ref: '#/components/schemas/starfish-editable-parameters'                
        required: true
      tags:
        - datasource
      responses:
        '200':
          content:
            application/json:
              schema:
                oneOf:
                  - $ref: '#/components/schemas/owm-datasource'
                  - $ref: '#/components/schemas/nwdaf-datasource'
                  - $ref: '#/components/schemas/ransim-datasource'
                  - $ref: '#/components/schemas/starfish-datasource'                  
                discriminator:
                  propertyName: datasource-type
                  mapping:
                    OpenWeatherMap: '#/components/schemas/owm-datasource'
                    NWDAF: '#/components/schemas/nwdaf-datasource'
                    RanSim: '#/components/schemas/ransim-datasource'
                    Starfish: '#/components/schemas/starfish-datasource'                    
          description: Configuration correctly updated.
        '400':
          description: Provided invalid configuration.
        '404':
          description: Datasource not found.
      operationId: updateTypedDatasourceById
      summary: >-
        Updates configuration of the datasource with identifier specified in the
        path.
#
    delete:
      tags:
        - datasource
      responses:
        '200':
          content:
            application/json:
              schema:
                type: integer
          description: Datasource deleted.
        '404':
          description: Datasource not found.
      operationId: deleteTypedDatasourceById
      summary: Deletes the datasource with identifier id.
    parameters:
      - name: type
        description: Datasource type.
        schema:
          $ref: '#/components/schemas/datasource-type'
        in: path
        required: true
      - name: id
        description: Identifier of the datasource to deal with.
        schema:
          type: string
          format: uuid
        in: path
        required: true
###############################################################################################
###############################################################################################
components:
  schemas:
    datasource-type: 
      description: >-
        Enum in order to list all the possible datasource type in ANChOR. Can be
        one of:
         - OpenweatherMap
         - NWDAF
         - Starfish
         - RanSim
#
      enum:
        - OpenWeatherMap
        - NWDAF
        - Starfish
        - RanSim
      type: string
###############################################################################################
    owm-metrics:
      description: >-
        Possible metric that can be retrieved by the OWM datasource. Possible
        values can be: 
        - cloudiness
        - humidity
        - pressure
        - raint
        - temperature
        - wind-degrees
        - wind-speed
#
      enum:
        - cloudiness
        - humidity
        - pressure
        - rain
        - temperature
        - wind-degrees
        - wind-speed
      type: string
###############################################################################################
    nwdaf-metrics:
      description: >-
        Possible metrics that the nwdaf datasource can retrieve. Possible values
        are: 
         - ul-volume
         - dl-volume
         - rtt
         - pdu-number
         - pkt-loss
#
      enum:
        - ul-volume
        - dl-volume
        - rtt
        - pdu-number
        - pkt-loss
###############################################################################################
    starfish-metrics:
      description: >-
        Possible metrics that the starfish datasource can retrieve. Possible values are:
        - traffic_counters
        - traffic_formats
        - kpis
      enum:
        - traffic_counters
        - traffic_formats
        - kpis
###############################################################################################
    ransim-metrics:
      description: >-
        Possible metrics that the ransim datasource can retrieve. Possible values are (afb stands for "active frequency ands"):
         - ue_positioning
         - gnb_positioning
         - network_configuration
         - resource_configuration
         - network_status
      enum:
         - ue_positioning
         - gnb_positioning
         - network_configuration
         - resource_configuration
         - network_status
###############################################################################################
    datasource:
      description: Info Model to deal with datasources
      type: object
      properties:
        datasource-type: 
              $ref: '#/components/schemas/datasource-type'
      discriminator:
        propertyName: datasource-type
###############################################################################################
    owm-editable-parameters:
      description: >-
        Parameters that can be modified inside a put request on an OWM datasource
#
      type: object
      properties:
        metrics:
          type: array
          items:
            $ref: '#/components/schemas/owm-metrics'
          description: >-
            List of the weather metrics to be retrieved by this datasource.
            If not specified, it will retrieve all the metrics.
#         
        topic-name:
          type: string
          description: >-
            Kafka topic in which metrics of this datasource will be published. If empty the metrics will be published in a Kafka topic named according a predefined pattern.
###############################################################################################
    nwdaf-editable-parameters:
      description: >-
        Parameters that can be modified inside a put request on an NWDAF datasource
#
      type: object
      properties:
        api-root:
          type: string
          description: >-
            Ip address of the api-root to which the HTTP requests must be
            issued.
#
        interval:
          description: The inter-request interval of the NWDAF datasource.
          type: string
        metrics:
          description: List of the metrics to be retrieved for this datasource.
          type: array
          items:
            $ref: '#/components/schemas/nwdaf-metrics'
        topic-name:
          type: string
          description: >-
            Name of the topic in which the requested metrics retrieved by
            this datasource will be published. If empty the topic name will
            follow a common pattern.
###############################################################################################
    starfish-editable-parameters:
      type: object
      properties:
        api-root:
          type: string
          description: Ip address of the api-root to which the HTTP requests must be issued.
        interval:
          description: The inter-request interval of the Starfish datasource.
          type: string
        metrics:
          description: List of the metrics to be retrieved by this datasource.
          type: array
          items:
            $ref: '#/components/schemas/starfish-metrics'
        topic-name:
          type: string
          description: >-
            Name of the topic in which the requested metrics retrieved by
            this datasource will be published. If empty the topic name will
            follow a common pattern.
###############################################################################################
    ransim-editable-parameters:
      description: >-
        Parameters that can be modified inside a put request on an RANSIM datasource
#
      type: object
      properties:
        metrics:
          description: List of the metrics to be retrieved for this datasource.
          type: array
          items:
            $ref: '#/components/schemas/ransim-metrics'
        remote-output-file-path:
          type: string
          description: "Path of the output file on the RAN simulator machine"
        remote-input-files-path:
          type: array
          description: "List of the paths of the input files on the RAN simulator machine"
          items:
            type: string
        topic-name:
          type: string
          description: >-
            Name of the topic in which the requested metrics retrieved by
            this datasource will be published. If empty the topic name will
            follow a common pattern.
        
        
# to be eventually updated by using the paths (used to modify the entrypoint.sh to build the pipes)
###############################################################################################
    owm-datasource:
      description: >-
        Info model of the OpenWeatherMap datasources. The interval is fixed to 1
        h since the OWM updates its data with this time frequeny.
#     
      allOf:
        - $ref: '#/components/schemas/datasource'
        - type: object
          required:
            - city-uid
          properties:
            id:
              type: string
              format: uuid
              description: >-
                Identifier of the OWM datasource
#
            city-uid:
              type: integer
              description: >-
                Identifier of the city whose current and forecast weather data
                must be retrieved.
#
            metrics:
              type: array
              items:
                $ref: '#/components/schemas/owm-metrics'
              description: >-
                List of the weather metrics to be retrieved by this datasource.
                If not specified, it will retrieve all the metrics.
#         
            topic-name:
              type: string
              description: >-
                Kafka topic in which metrics of this datasource will be published. If empty the metrics will be published in a Kafka topic named according a predefined pattern.
#                
###############################################################################################
    nwdaf-datasource:
      description: Info model related to the configuration of an NWDAF datasource
      allOf:
        - $ref: '#/components/schemas/datasource'
        - type: object
          properties:
            id:
              type: string
              format: uuid
              description: >-
                Identifier of the NWDAF datasource
#
            end-to-end-network-slice-uid:
              description: The UID of the E2E network slice
              type: string
            network-slice-subnet-uid:
              description: The UID of the network slice subnet in the NWDAF domain (mapped in the nsiId in the request). If not specified, anyslice=true 3GPP-compliant
              type: string
            flow-id:
              description: IDs of the NS flows whose metrics must be retrieved.
              type: array
              items:
                type: string
            api-root:
              description: >-
                Ip address of the api-root to which the HTTP requests must be
                issued.
              type: string
            interval:
              description: The inter-request interval of the NWDAF datasource.
              type: string
            metrics:
              description: List of the metrics to be retrieved for this datasource.
              type: array
              items:
                $ref: '#/components/schemas/nwdaf-metrics'
            topic-name:
              type: string
              description: >-
                Name of the topic in which the requested metrics retrieved by
                this datasource will be published. If empty the topic name will
                follow a common pattern.
###############################################################################################
    ransim-datasource:
      description: >- 
         Info model related to the configuration of the RAN simulator datasource.
#
      allOf:
        - $ref: '#/components/schemas/datasource'
        - type: object
          required:
            - remote-output-file-path
            - remote-input-files-path
          properties:
            id:
              type: string
              format: uuid
              description: "Identifier of the RANSim datasource"
            remote-output-file-path:
              type: string
              description: "Path of the output file on the RAN simulator machine"
            remote-input-files-path:
              type: array
              description: "List of the paths of the input files on the RAN simulator machine"
              items:
                type: string
            metrics:
              type: array
              items:
                $ref: '#/components/schemas/ransim-metrics'
            topic-name:
              type: string
##
##
## I am not foreseeing to add the tails on the TLEs files since their number is variable, their format is quite complex and contains lots of fields and finally we agreed with Fabio that the simulator will write the gNB position (so the TLE parameters are not useful to compute them)
###############################################################################################
    starfish-datasource:
      description: Info model related to the configuration of a Starfish datasource
      allOf:
        - $ref: '#/components/schemas/datasource'
        - type: object
          required:
            - end-to-end-network-slice-uid
            - network-slice-subnet-uid
          properties:
            id:
              type: string
              format: uuid
              description: Identifier of the Starfish datasource
            api-root:
              description: api-root to which the HTTP requests must be issued.
              type: string
            interval:
              description: The inter-request interval of the Starfish datasource.
              type: string
            end-to-end-network-slice-uid:
              description: The UID of the E2E network slice
              type: string
            network-slice-subnet-uid:
              description: The UID of the network slice subnet in the Starfish domain
              type: string
            metrics:
              description: List of the metrics to be retrieved for this datasource.
              type: array
              items:
                $ref: '#/components/schemas/starfish-metrics'
            topic-name:
              type: string
              description: >-
                Name of the topic in which the requested metrics retrieved by
                this datasource will be published. If empty the topic name will
                follow a common pattern.
###############################################################################################
tags:
  - name: datasource
    description: >-
      Tag related to the configuration of the resourced monitored by the different datasources.
servers:
  - url: http://configmanager:8888
    variables: {}
