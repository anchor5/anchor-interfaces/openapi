swagger: '2.0'
info:
  description: Vertical Service Management Function Catalgue and LCM API
  version: '1.0'
  title: ANCHOR CSMF API
  termsOfService: urn:tos
  contact: {}
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0
host: localhost:8099
basePath: /
tags:

  - name: VSB Catalogue API
    description: Vertical Service Blueprint Catalogue API
  - name: VSD Catalogue API
    description: Vertical Service Descriptor Catalogue API
  - name: VS LCM Management API
    description: Vertical Service LCM Management API
paths:
 
  /csmf/catalogue/vsblueprint:
    get:
      tags:
        - VSB Catalogue API
      summary: Get ALL the Vertical Service Blueprints
      operationId: getAllVsBlueprintsUsingGET
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
        
      responses:
        '200':
          description: List of all the Vertical Service Blueprints
          schema:
            type: array
            items:
              $ref: '#/definitions/VsBlueprintInfo'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
    post:
      tags:
        - VSB Catalogue API
      summary: Onboard a new Vertical Service Blueprint
      operationId: createVsBlueprintUsingPOST
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
       
        - in: body
          name: request
          description: request
          required: true
          schema:
            $ref: '#/definitions/OnBoardVsBlueprintRequest'
      responses:
        '201':
          description: The ID of the created Vertical Service Blueprint
          schema:
            type: string
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /csmf/catalogue/vsblueprint/{vsbId}:
    get:
      tags:
        - VSB Catalogue API
      summary: Get a Vertical Service Blueprint with a given ID
      operationId: getVsBlueprintUsingGET
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
      
        - name: vsbId
          in: path
          description: vsbId
          required: true
          type: string
      responses:
        '200':
          description: Vertical Service Blueprint with the given ID
          schema:
            $ref: '#/definitions/VsBlueprintInfo'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
    delete:
      tags:
        - VSB Catalogue API
      summary: Delete a Vertical Service Blueprint with a given ID
      operationId: deleteVsBlueprintUsingDELETE
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
        
        - name: vsbId
          in: path
          description: vsbId
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            type: object
        '204':
          description: Empty
          schema:
            $ref: '#/definitions/ResponseEntity'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
      deprecated: false
  /csmf/catalogue/vsdescriptor:
    get:
      tags:
        - VSD Catalogue API
      summary: Query ALL the Vertical Service Descriptor
      operationId: getAllVsDescriptorsUsingGET
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
        
      responses:
        '200':
          description: List of all the Vertical Service Descriptor of the user.
          schema:
            type: array
            items:
              $ref: '#/definitions/VsDescriptor'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
    post:
      tags:
        - VSD Catalogue API
      summary: Onboard a new Vertical Service Descriptor
      operationId: createVsDescriptorUsingPOST
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
        
        - in: body
          name: request
          description: request
          required: true
          schema:
            $ref: '#/definitions/OnboardVsDescriptorRequest'
      responses:
        '201':
          description: The ID of the created Vertical Service Descriptor.
          schema:
            type: string
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /csmf/catalogue/vsdescriptor/{vsdId}:
    get:
      tags:
        - VSD Catalogue API
      summary: Query a Vertical Service Descriptor with a given ID
      operationId: getVsDescriptorUsingGET
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
       
        - name: vsdId
          in: path
          description: vsdId
          required: true
          type: string
      responses:
        '200':
          description: Details of the Vertical Service Descriptor with the given ID
          schema:
            $ref: '#/definitions/VsDescriptor'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
    delete:
      tags:
        - VSD Catalogue API
      summary: Delete a Vertical Service Descriptor with the given ID
      operationId: deleteVsDescriptorUsingDELETE
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
       
        - name: vsdId
          in: path
          description: vsdId
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            type: object
        '204':
          description: Empty
          schema:
            $ref: '#/definitions/ResponseEntity'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
      deprecated: false
  /csmf/vslcm/vs:
    post:
      tags:
        - VS LCM Management API
      summary: instantiateVs
      operationId: instantiateVsUsingPOST
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
        
        - in: body
          name: request
          description: request
          required: true
          schema:
            $ref: '#/definitions/InstantiateVsRequest'
      responses:
        '200':
          description: OK
          schema:
            type: object
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /csmf/vslcm/vs/{vsiId}:
    get:
      tags:
        - VS LCM Management API
      summary: Get Vertical Service Instance 
      operationId: getVsInstanceUsingGET
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
        
        - name: vsiId
          in: path
          description: vsiId
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            type: object
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
    put:
      tags:
        - VS LCM Management API
      summary: Modify Vertical Service Instance
      operationId: modifyVsInstanceUsingPUT
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
        
        - in: body
          name: request
          description: request
          required: true
          schema:
            $ref: '#/definitions/ModifyVsRequest'
        - name: vsiId
          in: path
          description: vsiId
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            type: object
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
    delete:
      tags:
        - VS LCM Management API
      summary: Delete Vertical Service Instance
      operationId: purgeVsInstanceUsingDELETE
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
        
        - name: vsiId
          in: path
          description: vsiId
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            type: object
        '204':
          description: No Content
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
      deprecated: false
  /csmf/vslcm/vs/{vsiId}/configure:
    put:
      tags:
        - VS LCM Management API
      summary: Trigger Vertical Service Configuration
      operationId: configureVsInstanceUsingPUT
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
        - in: body
          name: request
          description: request
          required: true
          schema:
            $ref: '#/definitions/Day2ActionRequest'
        - name: vsiId
          in: path
          description: vsiId
          required: true
          type: string
      responses:
        '200':
          description: The Vertical Service Instances is configured
          schema:
            type: object
        '201':
          description: Created
        '400':
          description: The request contains elements impossible to process
          schema:
            $ref: '#/definitions/Error'
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: VS instance not found
          schema:
            $ref: '#/definitions/Error'
        '409':
          description: There is a conflict with the request
          schema:
            $ref: '#/definitions/Error'
        '500':
          description: Internal Server Error
          schema:
            $ref: '#/definitions/Error'
      deprecated: false
  /csmf/vslcm/vs/{vsiId}/terminate:
    post:
      tags:
        - VS LCM Management API
      summary: Terminate Vertical Service Instance
      operationId: terminateVsInstanceUsingPOST
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
       
        - name: vsiId
          in: path
          description: vsiId
          required: true
          type: string
      responses:
        '200':
          description: OK
          schema:
            type: object
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /csmf/vslcm/vsId:
    get:
      tags:
        - VS LCM Management API
      summary: Get all Vertical Service Instance IDs
      operationId: getAllVsInstancesIdUsingGET
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
        
      responses:
        '200':
          description: OK
          schema:
            type: object
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
  /csmf/vslcm/vsInstances:
    post:
      tags:
        - VS LCM Management API
      summary: et all Vertical Service Instances
      operationId: getAllVsInstancesUsingPOST
      consumes:
        - application/json
      produces:
        - '*/*'
      parameters:
        - name: authenticated
          in: query
          required: false
          type: boolean
        - name: authorities[0].authority
          in: query
          required: false
          type: string
       
        - in: body
          name: request
          description: request
          required: true
          schema:
            $ref: '#/definitions/GeneralizedQueryRequest'
      responses:
        '200':
          description: The Vertical Service Instances matching the filter params
          schema:
            type: array
            items:
              $ref: '#/definitions/VerticalServiceInstance'
        '201':
          description: Created
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
      deprecated: false
definitions:
  ApplicationMetric:
    type: object
    properties:
      interval:
        type: string
      metricCollectionType:
        type: string
        enum:
          - CUMULATIVE
          - DELTA
          - GAUGE
      metricId:
        type: string
      name:
        type: string
      topic:
        type: string
      unit:
        type: string
    title: ApplicationMetric
  Day2ActionRequest:
    type: object
    properties:
      configRuleName:
        type: string
      params:
        type: object
        additionalProperties:
          type: string
      vsiId:
        type: string
    title: Day2ActionRequest
  EMBBServiceParameters:
    title: EMBBServiceParameters
    allOf:
      - $ref: '#/definitions/SliceServiceParameters'
      - type: object
        properties:
          areaTrafficCapDL:
            type: integer
            format: int64
          areaTrafficCapUL:
            type: integer
            format: int64
          coverage:
            type: string
          expDataRateDL:
            type: integer
            format: int64
          expDataRateUL:
            type: integer
            format: int64
          uESpeed:
            type: integer
            format: int64
          userDensity:
            type: integer
            format: int64
        title: EMBBServiceParameters
  Error:
    type: object
    properties:
      cause:
        $ref: '#/definitions/Throwable'
      description:
        type: string
      error:
        type: integer
        format: int32
      localizedMessage:
        type: string
      message:
        type: string
      stackTrace:
        type: array
        items:
          $ref: '#/definitions/StackTraceElement'
      suppressed:
        type: array
        items:
          $ref: '#/definitions/Throwable'
    title: Error
  Filter:
    type: object
    properties:
      parameters:
        type: object
        additionalProperties:
          type: string
    title: Filter
  GeneralizedQueryRequest:
    type: object
    properties:
      attributeSelector:
        type: array
        items:
          type: string
      filter:
        $ref: '#/definitions/Filter'
    title: GeneralizedQueryRequest
  InstantiateVsRequest:
    type: object
    properties:
      description:
        type: string
      name:
        type: string
      notificationUrl:
        type: string
      tenantId:
        type: string
      userData:
        type: object
        additionalProperties:
          type: string
      vsdId:
        type: string
    title: InstantiateVsRequest
  ModifyVsRequest:
    type: object
    properties:
      tenantId:
        type: string
      vsdId:
        type: string
      vsiId:
        type: string
    title: ModifyVsRequest
  OnBoardTranslationRuleRequest:
    type: object
    properties:
      vsdNsdTranslationRuleList:
        type: array
        items:
          $ref: '#/definitions/VsdNstTranslationRule'
    title: OnBoardTranslationRuleRequest
  OnBoardVsBlueprintRequest:
    type: object
    properties:
      translationRules:
        type: array
        items:
          $ref: '#/definitions/VsdNstTranslationRule'
      vsBlueprint:
        $ref: '#/definitions/VsBlueprint'
    title: OnBoardVsBlueprintRequest
  OnboardVsDescriptorRequest:
    type: object
    properties:
      nestedVsd:
        type: object
        additionalProperties:
          $ref: '#/definitions/VsDescriptor'
      tenantId:
        type: string
      vsd:
        $ref: '#/definitions/VsDescriptor'
    title: OnboardVsDescriptorRequest
  Policy:
    type: object
    properties:
      policyType:
        type: string
        enum:
          - SER
          - CST
          - A_COV
      policyValue:
        type: string
        enum:
          - HIGH
          - MEDIUM
          - LOW
    title: Policy
  RemoteTenantInfo:
    type: object
    properties:
      host:
        type: string
      remoteTenantName:
        type: string
      remoteTenantPwd:
        type: string
    title: RemoteTenantInfo
  ResponseEntity:
    type: object
    properties:
      body:
        type: object
      statusCode:
        type: string
        enum:
          - '100'
          - '101'
          - '102'
          - '103'
          - '200'
          - '201'
          - '202'
          - '203'
          - '204'
          - '205'
          - '206'
          - '207'
          - '208'
          - '226'
          - '300'
          - '301'
          - '302'
          - '303'
          - '304'
          - '305'
          - '307'
          - '308'
          - '400'
          - '401'
          - '402'
          - '403'
          - '404'
          - '405'
          - '406'
          - '407'
          - '408'
          - '409'
          - '410'
          - '411'
          - '412'
          - '413'
          - '414'
          - '415'
          - '416'
          - '417'
          - '418'
          - '419'
          - '420'
          - '421'
          - '422'
          - '423'
          - '424'
          - '426'
          - '428'
          - '429'
          - '431'
          - '451'
          - '500'
          - '501'
          - '502'
          - '503'
          - '504'
          - '505'
          - '506'
          - '507'
          - '508'
          - '509'
          - '510'
          - '511'
      statusCodeValue:
        type: integer
        format: int32
    title: ResponseEntity
  ServiceConstraints:
    type: object
    properties:
      atomicComponentId:
        type: string
      canIncludeSharedElements:
        type: boolean
      nonPreferredProviders:
        type: array
        items:
          type: string
      preferredProviders:
        type: array
        items:
          type: string
      priority:
        type: string
        enum:
          - LOW
          - MEDIUM
          - HIGH
      prohibitedProviders:
        type: array
        items:
          type: string
      sharable:
        type: boolean
    title: ServiceConstraints
  Sla:
    type: object
    properties:
      id:
        type: integer
        format: int64
      slaConstraints:
        type: array
        items:
          $ref: '#/definitions/SlaVirtualResourceConstraint'
      slaStatus:
        type: string
        enum:
          - ENABLED
          - DISABLED
    title: Sla
  SlaVirtualResourceConstraint:
    type: object
    properties:
      location:
        type: string
      maxResourceLimit:
        $ref: '#/definitions/VirtualResourceUsage'
      scope:
        type: string
        enum:
          - CLOUD_RESOURCE
          - MEC_RESOURCE
          - GLOBAL_VIRTUAL_RESOURCE
    title: SlaVirtualResourceConstraint
  SliceServiceParameters:
    type: object
    discriminator: type
    title: SliceServiceParameters
  StackTraceElement:
    type: object
    properties:
      className:
        type: string
      fileName:
        type: string
      lineNumber:
        type: integer
        format: int32
      methodName:
        type: string
      nativeMethod:
        type: boolean
    title: StackTraceElement
  Tenant:
    type: object
    properties:
      allocatedResources:
        $ref: '#/definitions/VirtualResourceUsage'
      password:
        type: string
      remoteTenantInfos:
        type: array
        items:
          $ref: '#/definitions/RemoteTenantInfo'
      sla:
        type: array
        items:
          $ref: '#/definitions/Sla'
      username:
        type: string
      vsdId:
        type: array
        items:
          type: string
      vsiId:
        type: array
        items:
          type: string
    title: Tenant
  Throwable:
    type: object
    properties:
      cause:
        $ref: '#/definitions/Throwable'
      localizedMessage:
        type: string
      message:
        type: string
      stackTrace:
        type: array
        items:
          $ref: '#/definitions/StackTraceElement'
      suppressed:
        type: array
        items:
          $ref: '#/definitions/Throwable'
    title: Throwable
  TransferPolicyRequest:
    type: object
    properties:
      designer:
        type: string
      name:
        type: string
      pfId:
        type: string
      policy:
        $ref: '#/definitions/Policy'
      version:
        type: string
    title: TransferPolicyRequest
  URLLCServiceParameters:
    title: URLLCServiceParameters
    allOf:
      - $ref: '#/definitions/SliceServiceParameters'
      - type: object
        properties:
          cSAvailability:
            type: number
            format: float
          connDensity:
            type: number
            format: float
          e2eKatency:
            type: integer
            format: int32
          expDataRate:
            type: integer
            format: int32
          jitter:
            type: integer
            format: int32
          payloadSize:
            type: integer
            format: int32
          reliability:
            type: number
            format: float
          serviceDimension:
            type: string
          survivalTime:
            type: integer
            format: int32
          trafficDensity:
            type: string
        title: URLLCServiceParameters
  VerticalServiceInstance:
    type: object
    properties:
      description:
        type: string
      errorMessage:
        type: string
      name:
        type: string
      networkSliceId:
        type: string
      status:
        type: string
        enum:
          - INSTANTIATING
          - INSTANTIATED
          - UNDER_MODIFICATION
          - WAITING_FOR_RESOURCES
          - INSTANTIATING_REMOTE_VSS
          - INSTANTIATING_LOCAL_VSS
          - MODIFIED
          - TERMINATING
          - TERMINATED
          - FAILED
      tenantId:
        type: string
      userData:
        type: object
        additionalProperties:
          type: string
      vsdId:
        type: string
      vsiId:
        type: string
    title: VerticalServiceInstance
  VirtualResourceUsage:
    type: object
    properties:
      diskStorage:
        type: integer
        format: int32
      memoryRAM:
        type: integer
        format: int32
      vCPU:
        type: integer
        format: int32
    title: VirtualResourceUsage
  VsBlueprint:
    type: object
    properties:
      applicationMetrics:
        type: array
        items:
          $ref: '#/definitions/ApplicationMetric'
      atomicComponents:
        type: array
        items:
          $ref: '#/definitions/VsComponent'
      blueprintId:
        type: string
      configurableParameters:
        type: array
        items:
          type: string
      connectivityServices:
        type: array
        items:
          $ref: '#/definitions/VsbLink'
      description:
        type: string
      embbServiceCategory:
        type: string
        enum:
          - URBAN_MACRO
          - RURAL_MACRO
          - INDOOR_HOTSPOT
          - BROADBAND_ACCESS_IN_A_CROWD
          - DENSE_URBAN
          - BROADBAND_LIKE_SERVICES
          - HIGH_SPEED_TRAIN
          - HIGH_SPEED_VEHICLE
          - AIRPLANES_CONNECTIVITY
      endPoints:
        type: array
        items:
          $ref: '#/definitions/VsbEndpoint'
      interSite:
        type: boolean
      name:
        type: string
      parameters:
        type: array
        items:
          $ref: '#/definitions/VsBlueprintParameter'
      serviceSequence:
        type: array
        items:
          $ref: '#/definitions/VsbForwardingPathHop'
      sliceServiceType:
        type: string
        enum:
          - NONE
          - EMBB
          - URLLC
          - M_IOT
          - ENTERPRISE
          - NFV_IAAS
      urllcServiceCategory:
        type: string
        enum:
          - DISCRETE_AUTOMATION
          - PROCESS_AUTOMATION_REMOTE_CONTROL
          - PROCESS_AUTOMATION_MONITORING
          - ELECTRICITY_DISTRIBUTION_HIGH_VOLTAGE
          - ELECTRICITY_DISTRIBUTION_MEDIUM_VOLTAGE
          - INTELLIGENT_TRANSPORT_SYSTEMS_INFRASTRUCTURE_BACKHAUL
      version:
        type: string
    title: VsBlueprint
  VsBlueprintInfo:
    type: object
    properties:
      activeVsdId:
        type: array
        items:
          type: string
      name:
        type: string
      vsBlueprint:
        $ref: '#/definitions/VsBlueprint'
      vsBlueprintId:
        type: string
      vsBlueprintVersion:
        type: string
    title: VsBlueprintInfo
  VsBlueprintParameter:
    type: object
    properties:
      applicabilityField:
        type: string
      parameterDescription:
        type: string
      parameterId:
        type: string
      parameterName:
        type: string
      parameterType:
        type: string
    title: VsBlueprintParameter
  VsComponent:
    type: object
    properties:
      associatedVsbId:
        type: string
      compatibleSite:
        type: string
      componentId:
        type: string
      endPointsIds:
        type: array
        items:
          type: string
      imagesUrls:
        type: array
        items:
          type: string
      lifecycleOperations:
        type: object
        additionalProperties:
          type: string
      placement:
        type: string
        enum:
          - EDGE
          - CLOUD
      serversNumber:
        type: integer
        format: int32
      type:
        type: string
        enum:
          - SERVICE
          - FUNCTION
          - OTHER
    title: VsComponent
  VsDescriptor:
    type: object
    properties:
      associatedVsdId:
        type: string
      domainId:
        type: string
      managementType:
        type: string
        enum:
          - PROVIDER_MANAGED
          - TENANT_MANAGED
      name:
        type: string
      nestedVsdIds:
        type: object
        additionalProperties:
          type: string
      qosParameters:
        type: object
        additionalProperties:
          type: string
      serviceConstraints:
        type: array
        items:
          $ref: '#/definitions/ServiceConstraints'
      sla:
        $ref: '#/definitions/VsdSla'
      sliceServiceParameters:
        $ref: '#/definitions/SliceServiceParameters'
      version:
        type: string
      vsBlueprintId:
        type: string
      vsDescriptorId:
        type: string
    title: VsDescriptor
  VsbEndpoint:
    type: object
    properties:
      endPointId:
        type: string
      external:
        type: boolean
      management:
        type: boolean
      ranConnection:
        type: boolean
    title: VsbEndpoint
  VsbForwardingPathEndPoint:
    type: object
    properties:
      endPointId:
        type: string
      vsComponentId:
        type: string
    title: VsbForwardingPathEndPoint
  VsbForwardingPathHop:
    type: object
    properties:
      hopEndPoints:
        type: array
        items:
          $ref: '#/definitions/VsbForwardingPathEndPoint'
    title: VsbForwardingPathHop
  VsbLink:
    type: object
    properties:
      connectivityProperties:
        type: array
        items:
          type: string
      endPointIds:
        type: array
        items:
          type: string
      external:
        type: boolean
      name:
        type: string
    title: VsbLink
  VsdNstTranslationRule:
    type: object
    properties:
      blueprintId:
        type: string
      id:
        type: integer
        format: int64
      input:
        type: array
        items:
          $ref: '#/definitions/VsdParameterValueRange'
      nstId:
        type: string
    title: VsdNstTranslationRule
  VsdParameterValueRange:
    type: object
    properties:
      maxValue:
        type: integer
        format: int32
      minValue:
        type: integer
        format: int32
      parameterId:
        type: string
    title: VsdParameterValueRange
  VsdSla:
    type: object
    properties:
      availabilityCoverage:
        type: string
        enum:
          - AVAILABILITY_COVERAGE_HIGH
          - AVAILABILITY_COVERAGE_MEDIUM
          - UNDEFINED
      lowCostRequired:
        type: boolean
      serviceCreationTime:
        type: string
        enum:
          - SERVICE_CREATION_TIME_LOW
          - SERVICE_CREATION_TIME_MEDIUM
          - UNDEFINED
    title: VsdSla
  VsmfNotificationMessage:
    type: object
    properties:
      errors:
        type: string
        enum:
          - STATUS_TRANSITION
      nsiId:
        type: string
        format: uuid
      nsiNotifType:
        type: string
        enum:
          - STATUS_CHANGED
          - ERROR
      nsiStatus:
        type: string
        enum:
          - CREATED
          - INSTANTIATING
          - INSTANTIATED
          - CONFIGURING
          - TERMINATING
          - TERMINATED
          - FAILED
          - OTHER
    title: VsmfNotificationMessage

